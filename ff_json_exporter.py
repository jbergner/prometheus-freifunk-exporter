#!/usr/bin/env python3
"""
JSON exporter ng
"""

import os
import sys
import datetime as dt
import time
import argparse
import jsonpath_rw_ext as jpext
import yaml
import requests
import prometheus_client as promc

static_metrics_dict = {}
filter_metrics_dict = {}

def load_config_file(file_path):
    """
    Load the config file.
    """
    try:
        if not os.path.exists(file_path):
            raise FileNotFoundError(f"Config file not found: {file_path}")

        with open(file_path, encoding="utf-8") as file:
            config = yaml.safe_load(file)

        if not isinstance(config, dict):
            raise ValueError(f"Invalid YAML format in config file: {file_path}")

        return config

    except FileNotFoundError as exception_var:
        print(f"Error loading config file: {exception_var}")
        sys.exit(1)
    except ValueError as exception_var:
        print(f"Error loading config file: {exception_var}")
        sys.exit(1)


def process_return(return_val, return_type):
    """
    Process json return by type.
    (boolean, hex, number, time)
    """

    if return_type == 'number':
        return float(return_val)
    if return_type == 'boolean':
        if bool(return_val):
            return 1
        return 0
    if return_type == 'hex':
        ###Convert hex string to integer
        return int(return_val, 16)
    if return_type == 'timestring':
        ### Parse time string and convert to ticks
        return int(dt.datetime.strptime(return_val,"%Y-%m-%dT%H:%M:%S.%fZ").timestamp())

    ### Unknown type
    ### TODO FAIL
    return -1

def process_metric(yaml_definition, json_data):
    """
    Process current metric.
    """

    process_success = True
    ret_val = -1
    ret_name = "NN"
    ret_desc = ""

    if not 'name' in yaml_definition:
        process_success = False
    if not 'query' in yaml_definition:
        process_success = False
    if not 'type' in yaml_definition:
        process_success = False

    if process_success:
        ret_name = yaml_definition['name']

        if 'description' in yaml_definition:
            ret_desc = yaml_definition['description']

        if 'default' in yaml_definition:
            ret_val = yaml_definition['default']

        json_parser = jpext.parse(yaml_definition['query'])
        parse_results = [match.value for match in json_parser.find(json_data)]

        if not len(parse_results) == 1:
            process_success = False
        else:
            json_return = parse_results[0]
            ret_val = process_return(json_return, yaml_definition['type'])
            ###TODO: Ill-defined Type returns -1 instead of error.

    return [ret_name, ret_desc, ret_val]


def extract_filter_labels(labels_dict, json_data, labels_yaml):
    """
    Extract labels for filtered metrics.
    """

    extraction_success = True

    for label in labels_yaml:
        mandatory = False
        label_val = "DEFAULT"
        label_name = "DEFAULT"

        if not 'name' in label:
            extraction_success = False
        if not 'query' in label:
            extraction_success = False

        if 'mandatory' in label:
            mandatory = bool(label['mandatory'])

        if extraction_success:
            if 'default' in label:
                label_val = label['default']
            label_name = label['name']

            json_parser = jpext.parse(label['query'])
            parse_results = [match.value for match in json_parser.find(json_data)]

            if (not len(parse_results) == 1) and mandatory:
                extraction_success = False
            else:
                label_val = parse_results[0]
                labels_dict[label_name] = label_val

    if extraction_success:
        return labels_dict
    return {}


def process_current_query(query_yaml, query_name, failure_metric=None):
    """
    Process the current query.
    """

    query_fail = False

    query_labels = {}
    query_labels['json_query_id'] = query_name

    if not 'api_endpoint' in query_yaml:
        query_fail = True

    if not query_fail:
        request_response = requests.get(query_yaml['api_endpoint'], timeout=4)
        if not request_response.status_code == 200:
            query_fail = True

    if not query_fail:
        metrics_prefix = 'json_exporter_ng_'

        if 'metrics_prefix' in query_yaml:
            metrics_prefix = query_yaml['metrics_prefix']

        if 'static_metrics' in query_yaml:
            for current_metric_yaml in query_yaml['static_metrics']:
                return_list = process_metric(current_metric_yaml, request_response.json())
                if len(return_list) == 0:
                    query_fail = True
                else:
                    metric_name = metrics_prefix + return_list[0]

                if not query_fail:
                    if metric_name not in static_metrics_dict:
                        static_metrics_dict[metric_name] = promc.Gauge(
                                metric_name,
                                return_list[1],
                                list(query_labels)
                                )

                    static_metrics_dict[metric_name].labels(
                            *[query_labels[label] for label in query_labels]
                            ).set(
                                    return_list[2]
                                    )

        if 'node_filters' in query_yaml:
            for cur_filter in query_yaml['node_filters']:
                json_parser = jpext.parse(cur_filter)
                parse_results = [match.value for match in json_parser.find(request_response.json())]
                for reduced_json in parse_results:
                    filter_labels ={}
                    for label in query_labels:
                        filter_labels[label] = query_labels[label]
                    if 'labels' in query_yaml:
                        filter_labels = extract_filter_labels(
                                filter_labels,
                                reduced_json,
                                query_yaml['labels']
                                )
                        if not filter_labels:
                            query_fail = True

                    if not query_fail:
                        if 'metrics_by_filter' in query_yaml:
                            for current_metric_yaml in query_yaml['metrics_by_filter']:
                                return_list = process_metric(current_metric_yaml, reduced_json)
                                if len(return_list) == 0:
                                    query_fail = True

                                metric_name = metrics_prefix + return_list[0]

                                if metric_name not in filter_metrics_dict:
                                    label_keys = list(filter_labels)
                                    filter_metrics_dict[metric_name] = promc.Gauge(
                                            metric_name,
                                            return_list[1],
                                            label_keys
                                            )

                                filter_metrics_dict[metric_name].labels(
                                                *[filter_labels[label] for label in filter_labels]
                                                ).set(
                                                        return_list[2]
                                                        )

    if not failure_metric is None:
        if query_fail:
            failure_metric.inc()

### Main function
def main():
    """
    The main function of this exporter.
    """

    ### At what time was the script started
    script_initial_time = dt.datetime.utcnow()

    # Parse command line arguments
    parser = argparse.ArgumentParser(description='JSON API to Prometheus exporter.')
    parser.add_argument('--config', '-c',
                        dest='config_path',
                        required=True,
                        help='Path to the YAML configuration file')
    parser.add_argument('--port', '-p',
                        dest='network_port',
                        default=9111,
                        help='Network port where metrics shall be provided.',
                        type=int)
    parser.add_argument('--updatetime', '-u',
                        dest='update_time_interval',
                        default=5,
                        type=int,
                        help='Time between metric recreations.')

    args = parser.parse_args()

    if not args.config_path is None:
        config_yaml = load_config_file(args.config_path)
    else:
        print("Error. No config file provided.")
        sys.exit(2)

    if args.update_time_interval < 1:
        print(f"Warning. Update time interval {args.update_time_interval} cannot be used.")
        print("         (Must be an integer > 0.) Falling back to default.")
        args.update_time_interval = 5

    if args.network_port < 1024:
        print(f"Warning. Metwork port {args.network_port} cannot be used.")
        print("         (Must be an integer > 1023.) Falling back to default.")
        args.network_port = 9111

    if not 'queries' in config_yaml:
        print(f"No queries defined in config file \"{args.config_path}\".")
        sys.exit(4)

    promc.start_http_server(args.network_port)

    metric_runtime = promc.Gauge('json_exporter_ng_script_runtime',
                                 'Seconds, this script is running')
    metric_runtime.set((dt.datetime.utcnow() - script_initial_time).total_seconds())
    metric_illdefinedqueries = promc.Counter(
            'json_exporter_ng_ill_defined_queries',
            'Queries that are not properly defined in the config file.'
            )

    for current_query in config_yaml['queries']:
        process_current_query(config_yaml['queries'][current_query],
                              current_query,
                              failure_metric=metric_illdefinedqueries)



    while True:
        time.sleep(args.update_time_interval)

        metric_runtime.set((dt.datetime.utcnow() - script_initial_time).total_seconds())
        for current_query in config_yaml['queries']:
            process_current_query(config_yaml['queries'][current_query],
                                  current_query)

if __name__ == '__main__':
    main()
